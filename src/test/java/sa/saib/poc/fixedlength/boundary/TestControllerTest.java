// package sa.saib.poc.fixedlength.boundary;
//
// import lombok.extern.slf4j.Slf4j;
// import org.assertj.core.api.SoftAssertions;
// import org.junit.jupiter.api.AfterEach;
// import org.junit.jupiter.api.BeforeEach;
// import org.junit.jupiter.api.Test;
// import org.springframework.beans.factory.annotation.Autowired;
// import org.springframework.boot.test.context.SpringBootTest;
// import org.springframework.boot.test.web.client.TestRestTemplate;
// import org.springframework.boot.web.server.LocalServerPort;
// import org.springframework.http.ResponseEntity;
// import sa.saib.poc.fixedlength.entity.Currency;
// import sa.saib.poc.fixedlength.entity.CurrencyExchangeRate;
//
// import java.util.Arrays;
// import java.util.List;
//
// import static org.springframework.boot.test.context.SpringBootTest.WebEnvironment.RANDOM_PORT;
//
// @Slf4j
// @SpringBootTest(webEnvironment = RANDOM_PORT)
// class TestControllerTest {
//  @LocalServerPort private int port;
//
//  @Autowired private TestRestTemplate restTemplate;
//
//  SoftAssertions softAssert;
//
//  @BeforeEach
//  void setup() {
//    softAssert = new SoftAssertions();
//  }
//
//  @Test
//  void unmarshal() {
//    ResponseEntity<Currency> response =
//        this.restTemplate.getForEntity(
//            "http://localhost:" + port + "/unmarshal/10000000000000200003", Currency.class);
//    Currency body = response.getBody();
//    softAssert.assertThat(body).isNotNull();
//    softAssert.assertThat(body.getName()).isEqualTo("1000000000");
//    softAssert.assertThat(body.getCode()).isEqualTo("00002");
//    softAssert.assertThat(body.getValue()).isEqualTo("00003");
//  }
//
//  @Test
//  void marshal() {
//    List<CurrencyExchangeRate> exchangeRates =
//        Arrays.asList(new CurrencyExchangeRate("2.5", "USD"));
//    ResponseEntity<String> response =
//        this.restTemplate.postForEntity(
//            "http://localhost:" + port + "/marshal",
//            new Currency("1", "2", "3", exchangeRates),
//            String.class);
//    String body = response.getBody();
//    System.out.println("Response: \"" + body + "\"");
//    softAssert.assertThat(body).isNotNull();
//    softAssert.assertThat(body.substring(0, 20)).isEqualTo("         1    2    3");
//  }
//
//  @AfterEach
//  void teardown() {
//    softAssert.assertAll();
//  }
// }
