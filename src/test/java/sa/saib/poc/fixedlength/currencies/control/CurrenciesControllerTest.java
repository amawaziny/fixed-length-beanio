package sa.saib.poc.fixedlength.currencies.control;

import lombok.SneakyThrows;
import lombok.extern.slf4j.Slf4j;
import org.assertj.core.api.SoftAssertions;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import sa.saib.poc.fixedlength.currencies.entity.City;
import sa.saib.poc.fixedlength.currencies.entity.Country;
import sa.saib.poc.fixedlength.currencies.entity.Currency;
import sa.saib.poc.fixedlength.currencies.entity.CurrencyExchangeRate;

import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static org.springframework.boot.test.context.SpringBootTest.WebEnvironment.RANDOM_PORT;

@Slf4j
@SpringBootTest(webEnvironment = RANDOM_PORT)
class CurrenciesControllerTest {
  private SoftAssertions assertions;
  private static final String FIXED_LENGTH_CURRENCIES =
      "Saudi Riyal         SAR  2 1.258684  EGP  true 010220203.75      USD  false010220212 Egypt          EGY  2 Cairo          CAI  Alexandria     ALEX Saudi          SAU  1 Riyadh         RUH  \r\n";

  final CurrenciesController ctrl;

  @Autowired
  CurrenciesControllerTest(CurrenciesController ctrl) {
    this.ctrl = ctrl;
  }

  @BeforeEach
  void setup() {
    assertions = new SoftAssertions();
  }

  @Test
  @SneakyThrows
  void writeCurrencies() {
    List<Currency> currencies = new ArrayList<>();
    CurrencyExchangeRate egp =
        new CurrencyExchangeRate(
            new BigDecimal("1.258684"),
            "EGP",
            true,
            new SimpleDateFormat("ddMMyyyy").parse("01022020"));
    CurrencyExchangeRate usd =
        new CurrencyExchangeRate(
            new BigDecimal("3.75"),
            "USD",
            false,
            new SimpleDateFormat("ddMMyyyy").parse("01022021"));
    City cairo = new City("Cairo", "CAI");
    City alex = new City("Alexandria", "ALEX");
    Country eg = new Country("Egypt", "EGY", 2, Arrays.asList(cairo, alex));
    City riyadh = new City("Riyadh", "RUH");
    Country ksa = new Country("Saudi", "SAU", 1, List.of(riyadh));
    currencies.add(
        new Currency("Saudi Riyal", "SAR", 2, Arrays.asList(egp, usd), 2, Arrays.asList(eg, ksa)));
    String fixedLengthCurrencies = ctrl.writeCurrencies(currencies);
    assertions.assertThat(fixedLengthCurrencies).isEqualTo(FIXED_LENGTH_CURRENCIES);
  }

  @Test
  @SneakyThrows
  void readCurrencies() {
    List<Currency> currencies = ctrl.readCurrencies(FIXED_LENGTH_CURRENCIES);
    assertions.assertThat(currencies).hasSize(1);
    Currency c1 = currencies.get(0);
    assertions.assertThat(c1.getName()).isEqualTo("Saudi Riyal");
    assertions.assertThat(c1.getCode()).isEqualTo("SAR");

    assertions.assertThat(c1.getNumExchangeRates()).isEqualTo(2);
    CurrencyExchangeRate exchangeRate = c1.getExchangeRates().get(0);
    assertions.assertThat(exchangeRate.getExchangeRate()).isEqualTo(new BigDecimal("1.258684"));
    assertions.assertThat(exchangeRate.getActive()).isEqualTo(true);
    assertions.assertThat(exchangeRate.getTargetCurrency()).isEqualTo("EGP");
    assertions
        .assertThat(exchangeRate.getDate())
        .isEqualTo(new SimpleDateFormat("ddMMyyyy").parse("01022020"));
    exchangeRate = c1.getExchangeRates().get(1);
    assertions.assertThat(exchangeRate.getExchangeRate()).isEqualTo(new BigDecimal("3.75"));
    assertions.assertThat(exchangeRate.getActive()).isEqualTo(false);
    assertions.assertThat(exchangeRate.getTargetCurrency()).isEqualTo("USD");
    assertions
        .assertThat(exchangeRate.getDate())
        .isEqualTo(new SimpleDateFormat("ddMMyyyy").parse("01022021"));

    assertions.assertThat(c1.getNumCountries()).isEqualTo(2);
    Country country = c1.getCountries().get(0);
    assertions.assertThat(country.getCountryName()).isEqualTo("Egypt");
    assertions.assertThat(country.getCountryCode()).isEqualTo("EGY");
    assertions.assertThat(country.getNumCities()).isEqualTo(2);
    City city = country.getCities().get(0);
    assertions.assertThat(city.getCityName()).isEqualTo("Cairo");
    assertions.assertThat(city.getCityCode()).isEqualTo("CAI");
    city = country.getCities().get(1);
    assertions.assertThat(city.getCityName()).isEqualTo("Alexandria");
    assertions.assertThat(city.getCityCode()).isEqualTo("ALEX");

    country = c1.getCountries().get(1);
    assertions.assertThat(country.getCountryName()).isEqualTo("Saudi");
    assertions.assertThat(country.getCountryCode()).isEqualTo("SAU");
    city = country.getCities().get(0);
    assertions.assertThat(city.getCityName()).isEqualTo("Riyadh");
    assertions.assertThat(city.getCityCode()).isEqualTo("RUH");
  }

  @AfterEach
  void teardown() {
    assertions.assertAll();
  }
}
