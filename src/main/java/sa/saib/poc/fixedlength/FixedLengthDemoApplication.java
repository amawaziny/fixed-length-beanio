package sa.saib.poc.fixedlength;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.SpringBootConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
@SpringBootConfiguration
public class FixedLengthDemoApplication {

  public static void main(String[] args) {
    SpringApplication.run(FixedLengthDemoApplication.class, args);
  }
}
