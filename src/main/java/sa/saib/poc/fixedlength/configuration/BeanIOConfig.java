package sa.saib.poc.fixedlength.configuration;

import org.beanio.StreamFactory;
import org.beanio.builder.StreamBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import sa.saib.poc.fixedlength.currencies.entity.Currency;

@Configuration
public class BeanIOConfig {
  public static final String STREAM_CURRENCIES = "currencies";

  private static final String FORMAT_FIXED_LENGTH = "fixedlength";

  @Bean
  public StreamFactory streamFactory() {
    StreamFactory factory = StreamFactory.newInstance();
    factory.define(
        new StreamBuilder(STREAM_CURRENCIES).format(FORMAT_FIXED_LENGTH).addRecord(Currency.class));
    return factory;
  }
}
