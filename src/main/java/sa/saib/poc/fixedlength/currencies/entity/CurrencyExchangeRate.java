package sa.saib.poc.fixedlength.currencies.entity;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.beanio.annotation.Field;
import org.beanio.annotation.Record;

import java.math.BigDecimal;
import java.util.Date;

@Record
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class CurrencyExchangeRate {
  @Field(length = 10)
  BigDecimal exchangeRate;

  @Field(length = 5)
  String targetCurrency;

  @Field(length = 5)
  Boolean active;

  @Field(length = 8, format = "ddMMyyyy")
  Date date;
}
