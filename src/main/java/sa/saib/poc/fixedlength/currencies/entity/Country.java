package sa.saib.poc.fixedlength.currencies.entity;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.beanio.annotation.Field;
import org.beanio.annotation.Record;
import org.beanio.annotation.Segment;

import java.util.List;

@Record
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class Country {
  @Field(length = 15)
  String countryName;

  @Field(length = 5)
  String countryCode;

  @Field(length = 2)
  Integer numCities;

  @Segment(collection = List.class, minOccurs = 0, maxOccurs = 99, occursRef = "numCities")
  List<City> cities;
}
