package sa.saib.poc.fixedlength.currencies.control;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.extern.slf4j.Slf4j;
import org.beanio.BeanReader;
import org.beanio.BeanWriter;
import org.beanio.StreamFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import sa.saib.poc.fixedlength.currencies.entity.Currency;

import java.io.StringReader;
import java.io.StringWriter;
import java.util.ArrayList;
import java.util.List;

import static sa.saib.poc.fixedlength.configuration.BeanIOConfig.STREAM_CURRENCIES;

@Slf4j
@Controller
public class CurrenciesController {
  final StreamFactory factory;

  @Autowired
  public CurrenciesController(StreamFactory factory) {
    this.factory = factory;
  }

  public String writeCurrencies(List<Currency> currencies) {
    StringWriter stringWriter = new StringWriter();
    BeanWriter writer = factory.createWriter(STREAM_CURRENCIES, stringWriter);
    for (Currency currency : currencies) {
      writer.write(currency);
    }
    String currenciesFixedLength = stringWriter.toString();
    writer.close();
    log.info("Wrote Records" + currenciesFixedLength);
    return currenciesFixedLength;
  }

  public List<Currency> readCurrencies(String input) throws JsonProcessingException {
    BeanReader reader = factory.createReader(STREAM_CURRENCIES, new StringReader(input));
    Object record;
    List<Currency> currencies = new ArrayList<>();
    while ((record = reader.read()) != null) {
      log.info("Reading Records: " + new ObjectMapper().writeValueAsString(record));
      Currency currency = (Currency) record;
      currencies.add(currency);
    }
    reader.close();
    return currencies;
  }
}
