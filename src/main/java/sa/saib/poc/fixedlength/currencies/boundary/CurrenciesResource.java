package sa.saib.poc.fixedlength.currencies.boundary;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;
import sa.saib.poc.fixedlength.currencies.control.CurrenciesController;
import sa.saib.poc.fixedlength.currencies.entity.Currency;

import java.util.List;

import static org.springframework.http.MediaType.APPLICATION_JSON_VALUE;
import static org.springframework.http.MediaType.TEXT_PLAIN_VALUE;

@Slf4j
@RestController
public class CurrenciesResource {

  final CurrenciesController ctrl;

  @Autowired
  public CurrenciesResource(CurrenciesController ctrl) {
    this.ctrl = ctrl;
  }

  @PostMapping(value = "/unmarshal", consumes = TEXT_PLAIN_VALUE)
  public ResponseEntity<List<Currency>> unmarshal(@RequestBody String input) {
    try {
      return ResponseEntity.ok(ctrl.readCurrencies(input));
    } catch (Exception e) {
      log.error(e.getMessage(), e);
      return ResponseEntity.internalServerError().build();
    }
  }

  @PostMapping(value = "/marshal", consumes = APPLICATION_JSON_VALUE)
  public ResponseEntity<String> marshal(@RequestBody List<Currency> currencies) {
    try {
      return ResponseEntity.ok(ctrl.writeCurrencies(currencies));
    } catch (Exception e) {
      log.error(e.getMessage(), e);
      return ResponseEntity.internalServerError().build();
    }
  }
}
