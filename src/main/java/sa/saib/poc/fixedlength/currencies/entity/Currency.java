package sa.saib.poc.fixedlength.currencies.entity;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.beanio.annotation.Field;
import org.beanio.annotation.Record;
import org.beanio.annotation.Segment;

import java.util.List;

@Record
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class Currency {
  @Field(length = 20)
  String name;

  @Field(length = 5)
  String code;

  @Field(length = 2)
  Integer numExchangeRates;

  @Segment(
      collection = List.class,
      minOccurs = 0,
      maxOccurs = 99,
      lazy = true,
      occursRef = "numExchangeRates")
  List<CurrencyExchangeRate> exchangeRates;

  @Field(length = 2)
  Integer numCountries;

  @Segment(collection = List.class, minOccurs = 0, maxOccurs = 99, occursRef = "numCountries")
  List<Country> countries;
}
