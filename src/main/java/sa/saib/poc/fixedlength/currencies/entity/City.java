package sa.saib.poc.fixedlength.currencies.entity;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.beanio.annotation.Field;
import org.beanio.annotation.Record;

@Record
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class City {
  @Field(length = 15)
  String cityName;

  @Field(length = 5)
  String cityCode;
}
